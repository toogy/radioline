#include <stdbool.h>

#include "match.h"

#define EOS '\0'

bool match(const char *p, const char *s)
{
    for (; *p != EOS; ++p)
        switch (*p)
        {
            case '.':
                if (s == EOS)
                    return false;
                s++;
                break;
            case '?':
                return match(p + 1, s + 1) || match(p + 1, s);
            default:
                switch (*(++p))
                {
                    case '*':
                        while (*p == '*')
                            ++p;
                        while (*s != EOS)
                            if (match(p, s++))
                                return true;
                        if (*p == EOS && *s == EOS)
                            return true;
                        break;
                    default:
                        if (*(p - 1) != *s)
                            return false;
                        ++s;
                        --p;
                }
        }
    return *s == EOS;
}
