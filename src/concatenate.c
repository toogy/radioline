#include <sys/types.h>
#include <assert.h>
#include <stdlib.h>

#define EOS '\0'

static size_t strlen(const char *str)
{
    assert(str != NULL);
    size_t len = 0;
    for(; *str != EOS; ++str)
        ++len;
    return len;
}

const char* concatenate(const char** strings, int count)
{
    size_t total_len = 0;
    for (int i = 0; i < count; ++i)
        total_len += strlen(strings[i]);
    char *concatenated = malloc((total_len + 1) * sizeof (char));
    concatenated[total_len] = EOS;
    size_t index = 0;
    const char *tmp;
    for (int i = 0; i < count; ++i)
    {
        tmp = strings[i];
        while (*tmp != EOS)
        {
            concatenated[index] = *tmp;
            ++index;
            ++tmp;
        }
    }
    return concatenated;
}
