#!/usr/bin/env bash

LC_ALL=C

declare -A COUNTS

while read l; do
    i=0
    while (( i++ < ${#l} ))
    do
        char=$(expr substr "$l" $i 1)
        if [[ $char == [a-zA-Z] ]]; then
            if [ ${COUNTS[$char]+_} ]; then
                COUNTS[$char]=$((${COUNTS[$char]} + 1))
            else
                COUNTS[$char]=1
            fi
        fi
    done
done <$1

for k in "${!COUNTS[@]}"; do
    echo $k ${COUNTS[$k]}
done
