import re

class Word:
    def __init__(self, tm, word):
        self.tm = tm
        self.word = word

    def replace(self, new):
        for i in range(len(self.tm.words)):
            if self.tm.words[i] == self.word:
                self.tm.words[i] = new

    def remove(self):
        self.tm.words = list(filter((self.word).__ne__, self.tm.words))

    def insert(self, pos):
        self.tm.words.insert(pos, self.word)

class TextManipulator:
    def __init__(self, text):
        self.words = text.split()

    def __getitem__(self, word):
        res = []
        for i in range(len(self.words)):
            if self.words[i] == word:
                res.append(i)
        return res

    def __getattr__(self, word):
        return Word(self, word)

    def __repr__(self):
        return '"' + ' '.join(self.words) + '"'

    def __str__(self):
        return ' '.join(self.words)
