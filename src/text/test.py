from manip import *

manipulator = TextManipulator("un deux trois un deux trois")
print(repr(manipulator))
print(manipulator)
print(manipulator["deux"])
manipulator.deux.replace("second")
print(manipulator)
manipulator.un.remove()
print(manipulator)
manipulator.quatre.insert(2)
print(manipulator)
