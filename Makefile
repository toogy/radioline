RM = rm -vf

BUILDDIR = build
SRCDIR = src
INCLDIR = include
CHECKDIR = test

SRC = $(SRCDIR)/list.c $(SRCDIR)/vector.c
OBJ = $(SRC:$(SRCDIR)/%.c=$(BUILDDIR)/%.o)

CC = gcc
CFLAGS = -std=c99 -pedantic -Wall -Wextra -I./include
DBGFLAGS = -g3 -ggdb3
LDFLAGS = -fPIC

FLAGS = $(CFLAGS)

all: test_concatenate test_match test_text_manip test_count

test_text_manip:
	python src/text/test.py

test_count:
	./src/count.sh ./test/test.txt

test_%: FLAGS += $(DBGFLAGS)

test_%: $(BUILDDIR) $(SRCDIR)/%.c $(BUILDDIR)/%.o
	$(CC) $(FLAGS) $(CHECKDIR)/$@.c -o $@ $(@:test_%=$(BUILDDIR)/%.o)

$(BUILDDIR)/%.o: $(SRCDIR)/%.c
	$(CC) $(FLAGS) -c -o $@ $(@:$(BUILDDIR)/%.o=$(SRCDIR)/%.c)

$(BUILDDIR):
	mkdir -vp $(BUILDDIR)

clean:
	$(RM) -r $(BUILDDIR)
	$(RM) test_*
