# Concaténation de chaînes en C

Ecrire en C, sans utiliser string.h, la function

    const char* concatenate(const char** strings, int count)

qui prend un tableau de chaînes de caractères strings, et sa longueur count, et
qui renvoie la concaténation des toutes les chaînes. Attention à garder une
complexité linéaire.

Exemple:

    const char* strings[] = {"lala", "lolo", "lili"};
    const char* res = concatenate(strings, 3);
    printf("resultat: %s\n", res);
    >>> resultat: lalalololili

# Les regex simplifiées

On définit un langage simplifié de regex:

les patterns contiennes les caractères:

+ 'a'-'z': une lettre
+ '.': n'importe quelle lettre
+ '?': zéro ou une lettre (n'importe laquelle)
+ '*': autant de lettres que désiré (n'impote lesquelles)

Les chaînes à matcher continennes uniquement des lettres 'a'-'z'

Ecrire un fonction match (langage au choix, C conseillé) prenant un pattern et
une chaîne, et déterminant si le pattern match la chaîne.

Les performances ne sont pas importantes, prendra soin d'identifier les pièges
que pose une telle fonction.

Exemples:

```
match("abcd", "abcd")
>>> true
match("abcd", "abzcd")
>>> false
match("ab.cd", "abcd")
>>> false
match("ab.cd", "abzcd")
>>> true
match("ab?cd", "abcd")
>>> true
match("ab?cd", "abzcd")
>>> true
match("ab?cd", "abxyzcd")
>>> false
match("ab*cd", "abcd")
>>> true
match("ab*cd", "abzcd")
>>> true
match("ab*cd", "abxyzcd")
>>> true
match("ab*cd", "abzcdz")
>>> false
```

# Un manipulateur de texte en Python

Il est conseillé de lire la documentation de Python concernée
(https://docs.python.org/2/reference/datamodel.html)

Créer classe Python qui prend en paramètre un texte (des mots séparés par des
espaces)

    manipulator = TextManipulator("un deux trois un deux trois")

Il doit s'affiche correctement

```
repr(manipulator)
>>> "un deux trois un deux trois"
manipulator
>>> un deux trois un deux trois
```

La syntaxe manipulateur["mot"] doit donner la liste des positions de ce mot
(peut être vide)

```
manipulator["deux"]
>>> [1, 4]
```

La syntaxe manipulateur.mot doit permettre d'accéder à un certain nombre de
fonctions :

+ `replace` pour remplacer le mot par un autre

```
manipulator.deux.replace("second")
manipulator
>>> un second trois un second trois
```

+ `remove` supprime le mot

```
manipulator.un.remove()
manipulator
>>> second trois second trois
```

+ `insert` insère le mot à la position donnée

```
manipulator.quatre.insert(2)
manipulator
>>> second trois quatre second trois
```

Les performances ne sont importantes. On prendra soin de respecter la syntaxe
imposée.

# Compteur de lettres en bash

Ecrire un script bash simple qui compte le nombre d'occurence de chaque lettre.

Exemple:

    test.txt
        Ceci est un test
        pour une fonction bash.
    ./count.sh test.txt
    1 a
    1 b
    2 c
    1 C
    4 e
    1 f
    1 h
    2 i
    4 n
    3 o
    1 p
    1 r
    3 s
    4 t
    3 u

Utilisez astucieusement les commandes à votre disposition, évitez d'utiliser
d'autres langages.
