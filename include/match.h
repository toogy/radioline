#pragma once

#include <stdbool.h>

bool match(const char *pattern, const char *str);
