#include <stdio.h>

#include "concatenate.h"

int main(void)
{
    const char* strings[] = {"lala", "lolo", "lili"};
    const char* res = concatenate(strings, 3);
    printf("resultat: %s\n", res);
}
