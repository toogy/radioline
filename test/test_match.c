#include <assert.h>

#include "match.h"

int main(void)
{
    bool res;
    res = match("abcd", "abcd");
    assert(res == true);
    res = match("abcd", "abzcd");
    assert(res == false);
    res = match("ab.cd", "abcd");
    assert(res == false);
    res = match("ab.cd", "abzcd");
    assert(res == true);
    res = match("ab?cd", "abcd");
    assert(res == true);
    res = match("ab?cd", "abzcd");
    assert(res == true);
    res = match("ab?cd", "abxyzcd");
    assert(res == false);
    res = match("ab*cd", "abcd");
    assert(res == true);
    res = match("ab*cd", "abzcd");
    assert(res == true);
    res = match("ab*cd", "abxyzcd");
    assert(res == true);
    res = match("ab*cd", "abzcdz");
    assert(res == false);
}
